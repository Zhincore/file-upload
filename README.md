# fileupload

Overly simplified file upload with UUIDs.

## Usage

- **POST `/`** with multipart form data containing `file` field. Returns `id` of the uploaded file.
- **GET `/<id>`** to get that file.
