# use the official Bun image
# see all versions at https://hub.docker.com/r/oven/bun/tags
FROM oven/bun:1 AS base
WORKDIR /usr/src/app
ENV NODE_ENV "production"

# install dependencies into temp directory
# this will cache them and speed up future builds
FROM base AS install
WORKDIR /tmp/dev
COPY package.json bun.lockb ./
RUN bun install --frozen-lockfile


# copy production dependencies and source code into final image
FROM base AS release
WORKDIR /usr/src/app

ARG fileRoot=/data
ENV FILE_ROOT_PATH=$fileRoot

COPY . .
COPY --from=install /tmp/dev/node_modules ./node_modules

# run the app
EXPOSE 3000/tcp
ENTRYPOINT [ "bun", "run", "index.ts" ]
