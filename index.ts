import { type Serve } from "bun";
import Path from "node:path";
import { v4 as UUID } from "uuid";

const ROOT_PATH = process.env.FILE_ROOT_PATH || "data";

export default {
  async fetch(req) {
    if (req.method === "GET") {
      // Download
      const url = new URL(req.url);
      const path = Path.join(ROOT_PATH, url.pathname.slice(1));
      const file = Bun.file(path);

      if (await file.exists()) return new Response(file.stream());
      return new Response("Not found", { status: 404, statusText: "Not found" });
    } else if (req.method === "POST") {
      // Upload

      const formData = await req.formData();
      const file = formData.get("file") as File;
      if (!file) return new Response("No file", { status: 400 });

      const id = UUID();
      const path = Path.join(ROOT_PATH, id);
      await Bun.write(path, file);
      return new Response(id);
    }

    return new Response("Unsupported method", { status: 405 });
  },
} satisfies Serve;
